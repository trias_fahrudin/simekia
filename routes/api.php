<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/login', 'ApiController@login');
Route::get('/get-anak', 'ApiController@getAnak');
Route::get('/get-riwayat-imunisasi', 'ApiController@getRiwayatImunisasi');
Route::get('/get-riwayat-pemeriksaan', 'ApiController@getRiwayatPemeriksaan');
Route::post('/send_tokenid', 'ApiController@sendTokenId');

#
Route::get('/get-provinsi-list', 'ApiController@getProvinsi');
Route::get('/get-kabupaten-list','ApiController@getKabupaten');
Route::get('/get-kecamatan-list','ApiController@getKecamatan');
#
Route::get('/get-daftar-obat','ApiController@getDaftarObat');

#
Route::get('/get-jadwal-imunisasi','ApiController@getJadwalImunisasi');

#
Route::get('/get-data-bb','ApiController@getDataBb');
Route::get('/get-data-lk','ApiController@getDataLk');
Route::get('/get-data-tb','ApiController@getDataTb');

#
Route::get('get-perkembangan-detail','ApiController@getPerkembanganDetail');

#
Route::post('post-daftar-antrian','ApiController@postDaftarAntrian');
Route::get('get-antrian','ApiController@getAntrian');
Route::post('post-batalkan-antrian','ApiController@postBatalkanAntrian');
Route::get('get-antrian-detail','ApiController@getAntrianDetail');

#
Route::get('get-riwayat-notifikasi','ApiController@getRiwayatNotifikasi');

Route::post('post-foto-profile','ApiController@postFotoProfile');

Route::get('/update-antrian','ApiController@updateAntrian');