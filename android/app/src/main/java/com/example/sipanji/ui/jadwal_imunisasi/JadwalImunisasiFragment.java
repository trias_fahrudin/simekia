package com.example.sipanji.ui.jadwal_imunisasi;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.example.sipanji.MainActivity;
import com.example.sipanji.R;
import com.example.sipanji.ui.imunisasi_detail.ImunisasiDetailFragment;
import com.example.sipanji.util.SharedPrefManager;
import com.example.sipanji.util.api.BaseApiService;
import com.example.sipanji.util.api.UtilsApi;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.Objects;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class JadwalImunisasiFragment extends Fragment {

    private JadwalImunisasiAdapter jadwalImunisasiAdapter;
    private RecyclerView recyclerView;
    private SwipeRefreshLayout swipe;

    private Context mContext;
    private BaseApiService mBaseApiService;
    private SharedPrefManager sharedPrefManager;

    private ProgressDialog loading;

    private int pasien_id;
    private String pasien_tgl_lahir;
    private int pasien_usia;
    private String pasien_nama;

    @Override
    public void onAttach(Context context) {

        super.onAttach(context);
        mContext = context;
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_jadwal_imunisasi, container, false);
        swipe = root.findViewById(R.id.jadwal_imunisasi_swipeContainer);

        Bundle arguments = getArguments();
        if (arguments == null)
            Toast.makeText(getActivity(), "Arguments is NULL", Toast.LENGTH_LONG).show();
        else {
            pasien_id = getArguments().getInt("pasien_id", 0);
            pasien_tgl_lahir = getArguments().getString("pasien_tgl_lahir", "");
            pasien_usia = getArguments().getInt("pasien_usia", 0);
            pasien_nama = getArguments().getString("pasien_nama","");
        }

        mBaseApiService = UtilsApi.getAPIService();
        sharedPrefManager = new SharedPrefManager(mContext);

        androidx.appcompat.widget.Toolbar toolbar = requireActivity().findViewById(R.id.toolbar);
        toolbar.setTitle("Jadwal Imunisasi untuk " + pasien_nama);
        FloatingActionButton floatingActionButton = ((MainActivity) requireActivity()).getFloatingActionButton();
        if (floatingActionButton != null) {
            floatingActionButton.hide();

            floatingActionButton.setOnClickListener(view -> {

            });
        }

        swipe.setOnRefreshListener(() -> {
            swipe.setRefreshing(false);
            loadData();
        });

        loadData();

        return root;
    }

    private void loadData() {

        loading = ProgressDialog.show(mContext, null, "Mengambil data ...", true, false);

        mBaseApiService.getJadwalImunisasi(this.pasien_id, this.pasien_tgl_lahir, this.pasien_usia)
            .enqueue(new Callback<JadwalImunisasiModelList>() {
                @Override
                public void onResponse(@NonNull Call<JadwalImunisasiModelList> call, @NonNull Response<JadwalImunisasiModelList> response) {
                    if (response.isSuccessful()) {
                        loading.dismiss();
                        generateJadwalImunisasiList(Objects.requireNonNull(response.body()).getJadwalImunisasiArrayList());
                    } else {
                        loading.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<JadwalImunisasiModelList> call, Throwable t) {
                    Toasty.error(mContext, "Ada kesalahan!", Toast.LENGTH_LONG, true).show();
                    loading.dismiss();
                }
            });


    }

    private void generateJadwalImunisasiList(ArrayList<JadwalImunisasiModel> jadwalImunisasiArrayList) {

        recyclerView = requireView().findViewById(R.id.recycler_view_jadwalimunisasi_list);
        jadwalImunisasiAdapter = new JadwalImunisasiAdapter(jadwalImunisasiArrayList, this.pasien_usia);

        jadwalImunisasiAdapter.onBindCallBack = (jenis, viewHolder, position) -> {
            //ketika btn detail diklik
            if ("detail".equals(jenis)) {

                /*
                * holder.list_id = dataList.get(position).getList_id();
                  holder.list_nama = dataList.get(position).getList_nama();
                  holder.list_dilakukan = dataList.get(position).getList_dilakukan();
                * */
                Bundle bundle = new Bundle();
                bundle.putString("list_id", viewHolder.list_id); //3,1,2
                bundle.putString("list_nama", viewHolder.list_nama);//BCG,Hepatitis B-1,Polio-0
                bundle.putString("list_dilakukan", viewHolder.list_dilakukan);//3::2020-07-10,1::2020-07-07,2::2020-07-07
                bundle.putString("pasien_nama", pasien_nama);//3::2020-07-10,1::2020-07-07,2::2020-07-07

                ImunisasiDetailFragment fragment = new ImunisasiDetailFragment();
                fragment.setArguments(bundle);
                AppCompatActivity activity = (AppCompatActivity) getView().getContext();

                activity.getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.nav_host_fragment, fragment, ImunisasiDetailFragment.class.getSimpleName())
                    .addToBackStack(null)
                    .commit();


            }
            ;

            viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                }
            });

        };

        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getActivity(), 1);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(jadwalImunisasiAdapter);

    }


}
