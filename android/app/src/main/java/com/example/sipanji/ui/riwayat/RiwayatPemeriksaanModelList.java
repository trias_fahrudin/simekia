package com.example.sipanji.ui.riwayat;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class RiwayatPemeriksaanModelList {

    @SerializedName("riwayatPemeriksaanList")
    private ArrayList<RiwayatPemeriksaanModel> riwayatPemeriksaanList;

    public ArrayList<RiwayatPemeriksaanModel> getRiwayatPemeriksaanArrayList() {
        return riwayatPemeriksaanList;
    }

    public void setArraylList(ArrayList<RiwayatPemeriksaanModel> riwayatPemeriksaanArrayList) {
        this.riwayatPemeriksaanList = riwayatPemeriksaanArrayList;
    }

}
