package com.example.sipanji.ui.perkembangan;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.example.sipanji.MainActivity;
import com.example.sipanji.R;
import com.example.sipanji.ui.perkembangan_detail.PerkembanganDetailFragment;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PerkembanganFragment extends Fragment {

    @BindView(R.id.perkembangan_btnDetail)
    Button perkembanganBtnDetail;
    private Context context;
    private int pasien_id;
    private String pasien_jk;
    private String pasien_nama;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_perkembangan, container, false);
        ButterKnife.bind(this, root);


        Bundle arguments = getArguments();
        if (arguments == null)
            Toast.makeText(getActivity(), "Arguments is NULL", Toast.LENGTH_LONG).show();
        else {
            pasien_id = getArguments().getInt("pasien_id", 0);
            pasien_jk = getArguments().getString("pasien_jk","");
            pasien_nama = getArguments().getString("pasien_nama","");
        }

        Toolbar toolbar = getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle("Menu Pemeriksaan untuk " + pasien_nama);
        FloatingActionButton floatingActionButton = ((MainActivity) getActivity()).getFloatingActionButton();
        if (floatingActionButton != null) {
            floatingActionButton.hide();
        }

        PerkembanganTabsPagerAdapter tabsPagerAdapter = new PerkembanganTabsPagerAdapter(this.pasien_id, this.pasien_jk, this.context, getActivity().getSupportFragmentManager());

        ViewPager viewPager = root.findViewById(R.id.perkembangan_view_pager);
        viewPager.setAdapter(tabsPagerAdapter);

        TabLayout tabs = root.findViewById(R.id.perkembangan_tabs);
        viewPager.setOffscreenPageLimit(2);
        tabs.setupWithViewPager(viewPager);


        return root;

    }

    @OnClick(R.id.perkembangan_btnDetail)
    public void perkembangan_btnDetailOnClicked() {

        Bundle bundle = new Bundle();
        bundle.putInt("pasien_id", this.pasien_id);
        bundle.putString("pasien_nama",this.pasien_nama);

        PerkembanganDetailFragment fragment = new PerkembanganDetailFragment();
        fragment.setArguments(bundle);
        AppCompatActivity activity = (AppCompatActivity) getView().getContext();

        activity.getSupportFragmentManager()
            .beginTransaction()
            .replace(R.id.nav_host_fragment, fragment, PerkembanganDetailFragment.class.getSimpleName())
            .addToBackStack(null)
            .commit();
    }


}
