package com.example.sipanji.ui.jadwal_imunisasi;

public interface OnBindCallBack {
    void OnViewBind(String jenis, JadwalImunisasiAdapter.JadwalImunisasiViewHolder viewHolder, int position);
}
