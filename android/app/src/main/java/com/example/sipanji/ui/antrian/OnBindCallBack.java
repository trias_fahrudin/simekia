package com.example.sipanji.ui.antrian;

public interface OnBindCallBack {
    void OnViewBind(String jenis, AntrianAdapter.AntrianViewHolder viewHolder, int position);
}
