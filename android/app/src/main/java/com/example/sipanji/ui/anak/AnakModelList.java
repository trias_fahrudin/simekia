package com.example.sipanji.ui.anak;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class AnakModelList {

    @SerializedName("anakList")
    private ArrayList<AnakModel> anakList;

    public ArrayList<AnakModel> getAnakArrayList() {
        return anakList;
    }

    public void setAnakArraylList(ArrayList<AnakModel> anakArrayList) {
        this.anakList = anakArrayList;
    }

}
