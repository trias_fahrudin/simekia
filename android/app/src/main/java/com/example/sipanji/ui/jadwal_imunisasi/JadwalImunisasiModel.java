package com.example.sipanji.ui.jadwal_imunisasi;

import com.google.gson.annotations.SerializedName;

public class JadwalImunisasiModel {

    @SerializedName("usia")
    private int usia;
    @SerializedName("imunisasi_id")
    private String list_id;
    @SerializedName("imunisasi_nama")
    private String list_nama;
    @SerializedName("range_usia")
    private String range_usia;
    @SerializedName("imunisasi_status")
    private String list_status;
    @SerializedName("jml_belum_dilakukan")
    private int jml_belum_dilakukan;
    @SerializedName("imunisasi_dilakukan")
    private String list_dilakukan;
    @SerializedName("jml_sudah_dilakukan")
    private int jml_sudah_dilakukan;

    public int getJml_sudah_dilakukan() {
        return jml_sudah_dilakukan;
    }

    public void setJml_sudah_dilakukan(int jml_sudah_dilakukan) {
        this.jml_sudah_dilakukan = jml_sudah_dilakukan;
    }

    public int getJml_belum_dilakukan() {
        return jml_belum_dilakukan;
    }

    public void setJml_belum_dilakukan(int jml_belum_dilakukan) {
        this.jml_belum_dilakukan = jml_belum_dilakukan;
    }

    public int getUsia() {
        return usia;
    }

    public void setUsia(int usia) {
        this.usia = usia;
    }

    public String getList_id() {
        return list_id;
    }

    public void setList_id(String list_id) {
        this.list_id = list_id;
    }

    public String getList_nama() {
        return list_nama;
    }

    public void setList_nama(String list_nama) {
        this.list_nama = list_nama;
    }

    public String getRange_usia() {
        return range_usia;
    }

    public void setRange_usia(String range_usia) {
        this.range_usia = range_usia;
    }

    public String getList_status() {
        return list_status;
    }

    public void setList_status(String list_status) {
        this.list_status = list_status;
    }

    public String getList_dilakukan() {
        return list_dilakukan;
    }

    public void setList_dilakukan(String list_dilakukan) {
        this.list_dilakukan = list_dilakukan;
    }


}
