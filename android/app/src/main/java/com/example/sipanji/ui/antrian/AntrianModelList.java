package com.example.sipanji.ui.antrian;

import com.example.sipanji.ui.anak.AnakModel;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class AntrianModelList {

    @SerializedName("antrianList")
    private ArrayList<AntrianModel> antrianList;

    public ArrayList<AntrianModel> getArrayList() {
        return antrianList;
    }

    public void setArraylList(ArrayList<AntrianModel> antrianArrayList) {
        this.antrianList = antrianArrayList;
    }
}
