package com.example.sipanji.ui.antrian;

import com.google.gson.annotations.SerializedName;

public class AntrianModel {

    @SerializedName("id")
    private int id;
    @SerializedName("jenis")
    private String jenis;
    @SerializedName("tgl")
    private String tgl;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getJenis() {
        return jenis;
    }

    public void setJenis(String jenis) {
        this.jenis = jenis;
    }

    public String getTgl() {
        return tgl;
    }

    public void setTgl(String tgl) {
        this.tgl = tgl;
    }
}
