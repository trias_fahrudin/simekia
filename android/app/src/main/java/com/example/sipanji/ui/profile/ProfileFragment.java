package com.example.sipanji.ui.profile;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;

import com.example.sipanji.MainActivity;
import com.example.sipanji.R;
import com.example.sipanji.ui.anak.AnakModelList;
import com.example.sipanji.util.SharedPrefManager;
import com.example.sipanji.util.api.BaseApiService;
import com.example.sipanji.util.api.UtilsApi;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;


public class ProfileFragment extends Fragment {

    public final int REQUEST_CAMERA = 0;
    public final int SELECT_FILE = 1;
    @BindView(R.id.btn_choose_image)
    Button btnChooseImage;
    @BindView(R.id.profile_tvGender)
    TextView profileTvGender;
    @BindView(R.id.profile_tvNama)
    TextView profileTvNama;
    @BindView(R.id.profile_tvTglLahir)
    TextView profileTvTglLahir;
    @BindView(R.id.profile_tvTempatLahir)
    TextView profileTvTempatLahir;
    Intent intent;
    Uri fileUri;
    Bitmap bitmap, decoded;
    int PICK_IMAGE_REQUEST = 1;
    int bitmap_size = 60; // range 1 - 100
    Context mContext;
    @BindView(R.id.profile_imgView)
    ImageView profileImgView;
    private int pasien_id;
    private String pasien_jk;
    private String pasien_tgl_lahir;
    private int pasien_usia;
    private String pasien_nama;
    private String pasien_tempat_lahir;
    private String pasien_foto;

    BaseApiService mBaseApiService;
    ProgressDialog loading;

    /*
    *
    bundle.putInt("pasien_id", dataList.get(position).getId());
    bundle.putString("pasien_jk", dataList.get(position).getJk());
    bundle.putString("pasien_tgl_lahir", dataList.get(position).getTanggal_lahir());
    bundle.putInt("pasien_usia", dataList.get(position).getUsia());
    bundle.putString("pasien_nama",dataList.get(position).getNama());
    bundle.putString("pasien_tempat_lahir",dataList.get(position).getTempat_lahir());
    *
    *
    * */

    private static File getOutputMediaFile() {

        // External sdcard location
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "DeKa");

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.e("Monitoring", "Oops! Failed create Monitoring directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        File mediaFile;
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + "IMG_BeeSet_" + timeStamp + ".jpg");

        return mediaFile;
    }

    private static Bitmap rotateImage(Bitmap img, int degree) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degree);
        Bitmap rotatedImg = Bitmap.createBitmap(img, 0, 0, img.getWidth(), img.getHeight(), matrix, true);
        img.recycle();
        return rotatedImg;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.bind(this, root);

        Bundle arguments = getArguments();
        if (arguments == null)
            Toast.makeText(getActivity(), "Arguments is NULL", Toast.LENGTH_LONG).show();
        else {
            pasien_id = getArguments().getInt("pasien_id", 0);
            pasien_jk = getArguments().getString("pasien_jk", "");
            pasien_tgl_lahir = getArguments().getString("pasien_tgl_lahir", "");
            pasien_usia = getArguments().getInt("pasien_usia", 0);
            pasien_nama = getArguments().getString("pasien_nama", "");
            pasien_tempat_lahir = getArguments().getString("pasien_tempat_lahir", "");
            pasien_foto = getArguments().getString("pasien_foto", "");
        }

        profileTvNama.setText(pasien_nama.toString());
        profileTvGender.setText((pasien_jk.equals("L") ? "Laki-laki" : "Perempuan"));
        profileTvTempatLahir.setText(pasien_tempat_lahir);
        profileTvTglLahir.setText(pasien_tgl_lahir);

        Picasso.get().load(UtilsApi.BASE_URL + "data_file/" +  pasien_foto).into(profileImgView);


        mBaseApiService = UtilsApi.getAPIService();

        Toolbar toolbar = requireActivity().findViewById(R.id.toolbar);
        toolbar.setTitle("Profile");
        FloatingActionButton floatingActionButton = ((MainActivity) requireActivity()).getFloatingActionButton();
        if (floatingActionButton != null) {
            floatingActionButton.hide();

            floatingActionButton.setOnClickListener(view -> {

            });
        }

        return root;
    }

    @OnClick(R.id.btn_choose_image)
    public void onViewClicked() {

        pickImage();


    }

    private String getStringImage(Bitmap bmp) {

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, bitmap_size, baos);
        decoded = BitmapFactory.decodeStream(new ByteArrayInputStream(baos.toByteArray()));
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

    private void pickImage() {

        final CharSequence[] items = {
            "Ambil Foto",
            "Pilih Foto",
            "Batal"};

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle("Foto");
        builder.setIcon(R.mipmap.ic_launcher);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Ambil Foto")) {

                    fileUri = getOutputMediaFileUri();

                    File f = new File(Environment.getExternalStorageDirectory(), "temp.jpg");
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, FileProvider.getUriForFile(mContext, "com.example.sipanji.fileprovider", f));
                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    startActivityForResult(intent, REQUEST_CAMERA);

                } else if (items[item].equals("Pilih Foto")) {
                    intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_FILE);
                } else if (items[item].equals("Batal")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();

    }

    private Uri getOutputMediaFileUri() {
        return FileProvider.getUriForFile(mContext, "com.example.sipanji.fileprovider", Objects.requireNonNull(getOutputMediaFile()));
    }

    private void setToImageView(Bitmap bmp) {
        //compress image
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, bitmap_size, bytes);
        decoded = BitmapFactory.decodeStream(new ByteArrayInputStream(bytes.toByteArray()));
        // imageView.setImageBitmap(decoded);
        profileImgView.setImageBitmap(decoded);

        uploadFile();
    }

    private void uploadFile() {

        loading = ProgressDialog.show(mContext, null, "Mengambil data ...", true, false);
        mBaseApiService.postFotoProfile(this.pasien_id, getStringImage(decoded))
            .enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(@NotNull Call<ResponseBody> call, @NotNull Response<ResponseBody> response) {
                    if (response.isSuccessful()) {
                        loading.dismiss();

                        try {
                            JSONObject jsonObject = new JSONObject(response.body().string());
                            if (jsonObject.getString("error").equals("false")) {
                                Toast.makeText(mContext, "Upload file berhasil", Toast.LENGTH_SHORT).show();

                            } else {
                                String error_message = jsonObject.getString("error_msg");
                                Toast.makeText(mContext, error_message, Toast.LENGTH_SHORT).show();
                            }


                        } catch (IOException | JSONException e) {
                            e.printStackTrace();
                        }

                    } else {
                        loading.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Toasty.error(mContext, "Ada kesalahan!\n" + t.toString(), Toast.LENGTH_LONG, true).show();
                    loading.dismiss();
                }
            });
    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_CAMERA) {
                try {
                    Log.e("CAMERA", Objects.requireNonNull(fileUri.getPath()));
                    bitmap = rotateImage(BitmapFactory.decodeFile(fileUri.getPath()), 270);
                    setToImageView(getResizedBitmap(bitmap, 512));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (requestCode == SELECT_FILE && data != null && data.getData() != null) {
                try {
                    // mengambil gambar dari Gallery
                    bitmap = MediaStore.Images.Media.getBitmap(mContext.getContentResolver(), data.getData());
                    Log.d("Bitmap", bitmap.toString());
                    Log.d("Data:", data.getData().toString());
                    setToImageView(getResizedBitmap(bitmap, 512));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
