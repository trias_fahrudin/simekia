package com.example.sipanji.ui.signout;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.example.sipanji.LoginActivity;
import com.example.sipanji.R;
import com.example.sipanji.util.SharedPrefManager;

public class SignoutFragment extends Fragment {

    private Context mContext;
    SharedPrefManager sharedPrefManager;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_signout, container, false);
        sharedPrefManager = new SharedPrefManager(mContext);

        new AlertDialog.Builder(this.mContext)
            .setTitle("Keluar")
            .setMessage("Apakah anda yakin ingin keluar aplikasi?")
            .setPositiveButton("YA !", (dialog, which) -> {

                sharedPrefManager.saveBoolean(SharedPrefManager.SP_SUDAH_LOGIN, false);

                Intent myIntent = new Intent(getActivity(), LoginActivity.class);
                getActivity().startActivity(myIntent);

            }).setNegativeButton("Tidak", (dialog, which) -> {

        }).show();


        return root;
    }

}
