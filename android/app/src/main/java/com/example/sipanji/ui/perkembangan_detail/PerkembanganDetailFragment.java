package com.example.sipanji.ui.perkembangan_detail;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.example.sipanji.MainActivity;
import com.example.sipanji.R;
import com.example.sipanji.util.SharedPrefManager;
import com.example.sipanji.util.api.BaseApiService;
import com.example.sipanji.util.api.UtilsApi;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.Objects;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PerkembanganDetailFragment extends Fragment {

    private PerkembanganDetailAdapter adapter;
    private RecyclerView recyclerView;
    private SwipeRefreshLayout swipe;

    private Context mContext;
    private BaseApiService mBaseApiService;
    private SharedPrefManager sharedPrefManager;

    private ProgressDialog loading;

    private int pasien_id;
    private String pasien_nama;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_perkembangan_detail, container, false);
        swipe = root.findViewById(R.id.perkembangan_detail_swipeContainer);

        Bundle arguments = getArguments();
        if (arguments == null)
            Toast.makeText(getActivity(), "Arguments is NULL", Toast.LENGTH_LONG).show();
        else {
            pasien_id = getArguments().getInt("pasien_id", 0);
            pasien_nama = getArguments().getString("pasien_nama", "");
        }

        mBaseApiService = UtilsApi.getAPIService();
        sharedPrefManager = new SharedPrefManager(mContext);

        androidx.appcompat.widget.Toolbar toolbar = requireActivity().findViewById(R.id.toolbar);
        toolbar.setTitle("Perkembangan Detail untuk " + pasien_nama);
        FloatingActionButton floatingActionButton = ((MainActivity) requireActivity()).getFloatingActionButton();
        if (floatingActionButton != null) {
            floatingActionButton.hide();
            floatingActionButton.setOnClickListener(view -> {
            });
        }

        swipe.setOnRefreshListener(() -> {
            swipe.setRefreshing(false);
            loadData();
        });

        loadData();
        return root;
    }

    private void loadData() {

        loading = ProgressDialog.show(mContext, null, "Mengambil data ...", true, false);

        mBaseApiService.getPerkembanganDetail(this.pasien_id)
            .enqueue(new Callback<PerkembanganDetailModelList>() {
                @Override
                public void onResponse(@NonNull Call<PerkembanganDetailModelList> call, @NonNull Response<PerkembanganDetailModelList> response) {
                    if (response.isSuccessful()) {
                        loading.dismiss();
                        generateRecyclerList(Objects.requireNonNull(response.body()).getArrayList());
                    } else {
                        loading.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<PerkembanganDetailModelList> call, Throwable t) {
                    Toasty.error(mContext, "Ada kesalahan!", Toast.LENGTH_LONG, true).show();
                    loading.dismiss();
                }
            });


    }

    private void generateRecyclerList(ArrayList<PerkembanganDetailModel> modelList) {

        recyclerView = requireView().findViewById(R.id.recycler_view_perkembangandetail_list);
        adapter = new PerkembanganDetailAdapter(modelList);

        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getActivity(), 1);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

    }

}
