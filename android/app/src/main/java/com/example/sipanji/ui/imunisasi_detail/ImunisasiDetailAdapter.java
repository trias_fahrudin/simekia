package com.example.sipanji.ui.imunisasi_detail;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sipanji.R;

import java.util.ArrayList;
import java.util.List;

public class ImunisasiDetailAdapter extends RecyclerView.Adapter {

    private List<ImunisasiDetailModel> models = new ArrayList<>();


    public ImunisasiDetailAdapter(final List<ImunisasiDetailModel> viewModels) {
        if (viewModels != null) {
            this.models.addAll(viewModels);
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.recycler_detail_imunisasi_list, parent, false);
        return new ImunisasiDetailAdapter.ImunisasiDetailViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ((ImunisasiDetailViewHolder) holder).bindData(models.get(position));
    }

    @Override
    public int getItemCount() {
        return models.size();
    }

    @Override
    public int getItemViewType(final int position) {
        return R.layout.recycler_detail_imunisasi_list;
    }

    private class ImunisasiDetailViewHolder extends RecyclerView.ViewHolder {

        private TextView tvDetailImunisasi_namaImunisasi;
        private TextView tvDetailImunisasi_tgl;

        public ImunisasiDetailViewHolder(View view) {
            super(view);
            tvDetailImunisasi_namaImunisasi = (TextView) itemView.findViewById(R.id.tvDetailImunisasi_namaImunisasi);
            tvDetailImunisasi_tgl = (TextView) itemView.findViewById(R.id.tvDetailImunisasi_Tgl);
        }

        public void bindData(final ImunisasiDetailModel viewModel) {
            tvDetailImunisasi_namaImunisasi.setText(viewModel.getNama_imunisasi());
            tvDetailImunisasi_tgl.setText(viewModel.getTanggal());
        }
    }
}
