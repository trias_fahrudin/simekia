package com.example.sipanji.ui.antrian;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import com.example.sipanji.MainActivity;
import com.example.sipanji.R;
import com.example.sipanji.util.api.BaseApiService;
import com.example.sipanji.util.api.UtilsApi;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AntrianPendaftaranForm extends Fragment {

    @BindView(R.id.antrianPendaftaranForm_spJenis)
    Spinner spJenis;
    @BindView(R.id.antrianPendaftaranForm_etTanggal)
    TextInputEditText etTanggal;
    @BindView(R.id.antrianPendaftaranForm_btnSubmit)
    Button antrianPendaftaranFormBtnSubmit;
    @BindView(R.id.antrianPendaftaranForm_tvNama)
    TextView tvNama;
    @BindView(R.id.antrianPendaftaranForm_tvTglLahir)
    TextView tvTglLahir;
    private Context mContext;
    private BaseApiService mBaseApiService;
    private ProgressDialog loading;
    private static int pasien_id;
    String date = "";
    private String pasien_tgl_lahir;
    private int pasien_usia;
    private String pasien_nama;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_antrian_pendaftaran_form, container, false);
        ButterKnife.bind(this, root);

        mBaseApiService = UtilsApi.getAPIService();

        androidx.appcompat.widget.Toolbar toolbar = requireActivity().findViewById(R.id.toolbar);
        toolbar.setTitle("Form Pendaftaran Antrian");
        FloatingActionButton floatingActionButton = ((MainActivity) requireActivity()).getFloatingActionButton();
        if (floatingActionButton != null) {
            floatingActionButton.hide();
            floatingActionButton.setOnClickListener(view -> {

            });
        }

        Bundle arguments = getArguments();
        if (arguments == null)
            Toast.makeText(getActivity(), "Arguments is NULL", Toast.LENGTH_LONG).show();
        else {
            pasien_id = getArguments().getInt("pasien_id", 0);
            pasien_tgl_lahir = getArguments().getString("pasien_tgl_lahir", "");
            pasien_usia = getArguments().getInt("pasien_usia", 0);
            pasien_nama = getArguments().getString("pasien_nama", "");
        }

        tvNama.setText(this.pasien_nama);
        tvTglLahir.setText(String.format("%s (%d Bulan)",pasien_tgl_lahir.toString(),pasien_usia));

        etTanggal.setOnClickListener(view -> {

            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd"); // Make sure user insert date into edittext in this format.
            Date dateObject;
            try {
                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                Date now = new Date();
                dateObject = formatter.parse(dateFormat.format(now));

                date = new SimpleDateFormat("yyyy-MM-dd").format(dateObject);

                String[] items1 = date.split("-");
                String y1 = items1[0];
                String m1 = items1[1];
                String d1 = items1[2];
                int d = Integer.parseInt(d1);
                int m = Integer.parseInt(m1);
                int y = Integer.parseInt(y1);

                DatePickerDialog datePicker = new DatePickerDialog(mContext, (view1, year, monthOfYear, dayOfMonth) -> {
                    String date = String.valueOf(year) + "-" + String.format("%02d", monthOfYear + 1) + "-" + String.format("%02d", dayOfMonth);
                    etTanggal.setText(date);
                }, y, m - 1, d);
                datePicker.show();

            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                Log.i("Exception is: ", e.toString());
            }
        });

        return root;
    }


    @OnClick(R.id.antrianPendaftaranForm_btnSubmit)
    public void btnSubmitOnClicked() {

        new AlertDialog.Builder(mContext)
            .setTitle("Simpan Pendaftaran")
            .setMessage("Apakah data yang anda masukkan sudah benar?")
            .setPositiveButton("Ya", (dialog, which) -> {
                updateJurnal();
            }).setNegativeButton("Batal", null).show();

    }

    private void updateJurnal() {
        loading = ProgressDialog.show(mContext, null, "Menyimpan data, Mohon tunggu...", true, false);
        mBaseApiService.postPendaftaranAntrian(
            spJenis.getSelectedItem().toString(),
            etTanggal.getText().toString(),
            this.pasien_id
        ).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    loading.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        if (jsonObject.getString("error").equals("false")) {

                            Toasty.success(mContext, "Data berhasil disimpan", Toast.LENGTH_SHORT).show();

                            Bundle bundle = new Bundle();
                            bundle.putInt("pasien_id", AntrianPendaftaranForm.pasien_id);

                            AntrianFragment fragment = new AntrianFragment();
                            fragment.setArguments(bundle);
                            AppCompatActivity activity = (AppCompatActivity) getView().getContext();

                            activity.getSupportFragmentManager()
                                .beginTransaction()
                                .replace(R.id.nav_host_fragment, fragment, AntrianFragment.class.getSimpleName())
                                .addToBackStack(null)
                                .commit();


                        } else {
                            String error_message = jsonObject.getString("error_msg");
                            Toasty.error(mContext, error_message, Toast.LENGTH_SHORT).show();
                        }


                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    loading.dismiss();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("debug", "onFailure: ERROR > " + t.toString());
                loading.dismiss();
            }
        });
    }
}
