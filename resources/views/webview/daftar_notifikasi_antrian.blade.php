@extends('webview.layout')
@section('content')
 	<!-- @foreach ($daftarAntrian as $da)
        @if ($da->sisa_hari > 0 )
            <div class="alert alert-success alert-dismissible" style="margin: 5px">
                {{-- <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> --}}
                <h5><i class="icon fas fa-check"></i> Pengingat Kunjungan</h5>
                Kunjungan ananda {{ $da->nama }} untuk {{ $da->jenis }} dijadwalkan {{ $da->sisa_hari }} hari lagi
            </div>            
        @else
            <div class="alert alert-warning alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h5><i class="icon fas fa-exclamation-triangle"></i> Pengingat Kunjungan</h5>
                Kunjungan ananda {{ $da->nama }} untuk {{ $da->jenis }} dijadwalkan hari ini !<br/> Jangan sampai lupa ya ayah dan bunda
            </div>            
        @endif 
    @endforeach	
    <hr> -->
    @foreach ($imunisasi_list as $im)
        @if ($im['sisa_hari'] > 0 )
            <div class="alert alert-success alert-dismissible" style="margin: 5px">
                {{-- <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> --}}
                <h5><i class="icon fas fa-check"></i> Pengingat Imunisasi</h5>
                Jangan lupa ayah dan bunda, imunisasi anak usia {{ $im['usia_imunisasi'] }} bulan untuk ananda {{ $im['nama'] }} kurang {{ $im['sisa_hari'] }} hari lagi
            </div>            
        @else
            <div class="alert alert-warning alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h5><i class="icon fas fa-exclamation-triangle"></i> Pengingat Imunisasi</h5>
                Jangan lupa ayah dan bunda, imunisasi anak usia {{ $im['usia_imunisasi'] }} bulan untuk ananda {{ $im['nama'] }} jatuh pada hari ini.
            </div>            
        @endif 
    @endforeach	
@endsection