<script>
	

	
		var lk_u_0 = [{{ $lk_u_min_3_sd }} ];
	    var lk_u_1 = [{{ $lk_u_min_2_sd }} ];
	    var lk_u_2 = [{{ $lk_u_min_1_sd }}];
	    var lk_u_3 = [{{ $lk_u_median }}];
	    var lk_u_4 = [{{ $lk_u_plus_1_sd }}];
	    var lk_u_5 = [{{ $lk_u_plus_2_sd }}];
	    var lk_u_6 = [{{ $lk_u_plus_3_sd }}];

	    var lk_u_anak = [{{ $lk_u_array_anak }}];
	    

	    var plot_lk_u = $.jqplot("chart_lk_u", [lk_u_0, lk_u_1, lk_u_2, lk_u_3, lk_u_4, lk_u_5, lk_u_6, lk_u_anak], {
	        title: "Grafik Lingkar Kepala Menurut Usia",

	        axesDefaults: {
	            // pad: 0.5
	        },
	        axes:{
	          xaxis:{
	            min : 0,
	            max : 60,
	            tickInterval: 10,
	           label:'Umur (Bulan)',
	            labelRenderer: $.jqplot.CanvasAxisLabelRenderer
	          },
	          yaxis:{
	            min:0,
	            tickInterval: 4,
	           label:'Lingkar Kepala (Cm)',
	            labelRenderer: $.jqplot.CanvasAxisLabelRenderer
	          }
	        },
	        //////
	        // Use the fillBetween option to control fill between two
	        // lines on a plot.
	        //////
	        fillBetween: {
	            // series1: Required, if missing won't fill.
	            series1: [0,1,2,3,4,5],
	            // series2: Required, if  missing won't fill.
	            series2: [1,2,3,4,5,6],
	            // color: Optional, defaults to fillColor of series1.
	            color: ["rgba(255,215,0, 0.7)","rgba(253,253,97, 0.7)","rgba(69,139,116,0.7)","rgba(69,139,116,0.7)","rgba(253,253,97, 0.7)","rgba(255,215,0, 0.7)"],
	            // baseSeries:  Optional.  Put fill on a layer below this series
	            // index.  Defaults to 0 (first series).  If an index higher than 0 is
	            // used, fill will hide series below it.
	            // baseSeries: 0,
	            // fill:  Optional, defaults to true.  False to turn off fill.
	            fill: true
	        },

	        // seriesColors:['#cd5c5c', '#21252b', '#21252b', '#21252b', '#21252b','#21252b','#21252b','#34B7EA'],
	        seriesDefaults: {
	            rendererOptions: {
	                //////
	                // Turn on line smoothing.  By default, a constrained cubic spline
	                // interpolation algorithm is used which will not overshoot or
	                // undershoot any data points.
	                //////
	                smooth: true
	            },
	            // showMarker: false,
	            lineWidth: 1,
	        },
	        series:[
	          {
	            showMarker: false,
	            lineWidth: 1.5,
	            color : '#cd5c5c'
	          },
	          {
	            showMarker: false,
	            lineWidth: 0.5,
	            color :  '#000000'
	          },
	          {
	            showMarker: false,
	            lineWidth: 0.5,
	            color :  '#000000'
	          },
	          {
	            showMarker: false,
	            lineWidth: 0.5,
	            color :  '#000000'
	          },
	          {
	            showMarker: false,
	            lineWidth: 0.5,
	            color :  '#000000'
	          },
	          {
	            showMarker: false,
	            lineWidth: 0.5,
	            color :  '#000000'
	          },
	          {
	            showMarker: false,
	            lineWidth: 0.5,
	            color :  '#000000'
	          },
	          {
	            // lineWidth: 3,
	            showMarker: true,
	            color :  '#000000',
	            showLine : false,
	            markerOptions: { size: 7, style:"x" }
	          }
	        ]
	    });  	

</script>