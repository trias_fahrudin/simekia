@extends('dokter.layout')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
   <div class="container-fluid">
      <div class="row mb-2">
         <div class="col-sm-6">
            <h1>Data Jenis Imunisasi</h1>
         </div>
         <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
               <li class="breadcrumb-item"><a href="{{ URL::to('/dokter') }}">Home</a></li>
               <li class="breadcrumb-item active">Data Jenis Imunisasi</li>
            </ol>
         </div>
      </div>
   </div>
   <!-- /.container-fluid -->
</section>
<section class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-12">
            <div class="card">
               <div class="card-header container-fluid">
                  <div class="row">
                     <div class="col-md-10">
                        <h3 class="card-title">Data Jenis Imunisasi</h3>
                     </div>
                     <div class="col-md-2">
                        <a href="{{ URL::to('/dokter/jenis-imunisasi-tambah') }}" class="btn btn-primary float-right">Tambah Data</a>              
                     </div>
                  </div>
               </div>
               <!-- /.card-header -->
               <div class="card-body">
                  <table id="dataTables" class="table table-bordered table-striped">
                     <thead>
                        <tr>
                           <th>Usia Pemberian</th>
                           <th>Nama Imunisasi</th>  
                           <th>Toleransi</th>                           
                           <th></th>
                        </tr>
                     </thead>
                     <tbody>
                        @foreach($imunisasi as $o)
                        <tr>
                           <td>{{ $o->usia }}</td>
                           <td>{{ $o->nama }}</td>
                           <td>{{ $o->max_toleransi }}&nbsp;Hari</td>
                           <td>
                              <a href="{{ URL::to('/dokter/jenis-imunisasi-edit/' . $o->id) }}" class="btn btn-warning">Edit</a>
                              <a href="{{ URL::to('/dokter/jenis-imunisasi-hapus/' . $o->id) }}" onclick="return confirm('Apakah anda yakin?');" class="btn btn-danger">Hapus</a>
                           </td>
                        </tr>
                        @endforeach
                     </tbody>
                  </table>
               </div>
               <!-- /.card-body -->
            </div>
            <!-- /.card -->
         </div>
      </div>
   </div>
</section>
<script>
   $(function () {
     
     $('#dataTables').DataTable({
       "paging": true,
       "lengthChange": false,
       "searching": true,
       "ordering": true,
       "info": true,
       "autoWidth": false,
       "responsive": true,
     });
   });
</script>
@endsection