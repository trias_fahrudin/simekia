@extends('dokter.layout')
@section('content')
	<!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Edit Data Jenis Imunisasi</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ URL::to('/dokter') }}">Home</a></li>
              <li class="breadcrumb-item"><a href="{{ URL::to('/dokter/jenis-imunisasi') }}">Data Jenis Imunisasi</a></li>
              <li class="breadcrumb-item active">Edit</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Form Data Jenis Imunisasi</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" method="POST" action="{{ URL::to('/dokter/jenis-imunisasi-edit/' . $imunisasi->id) }}">
                {{ csrf_field() }}
                <div class="card-body">
                  <div class="form-group">
                    <label>Nama</label>
                    <input type="text" class="form-control" placeholder="" name="nama" value="{{ old('nama') ?? trim($imunisasi->nama) ?? '' }}">
                     @if($errors->has('nama'))
                          <div class="text-danger">
                              {{ $errors->first('nama')}}
                          </div>
                     @endif
                  </div>
                  
                  
                  <div class="form-group">
                    <label>Usia</label>
                    <select class="form-control" name="usia">
                      <option value="0" {{ ((old('usia') ?? $imunisasi->usia ?? '0') == '0') ? 'selected' : '' }}>Usia 0</option>
                      <option value="2" {{ ((old('usia') ?? $imunisasi->usia ?? '2') == '2') ? 'selected' : '' }}>Usia 2 Bulan</option>
                      <option value="3" {{ ((old('usia') ?? $imunisasi->usia ?? '3') == '3') ? 'selected' : '' }}>Usia 3 Bulan</option>
                      <option value="4" {{ ((old('usia') ?? $imunisasi->usia ?? '4') == '4') ? 'selected' : '' }}>Usia 4 Bulan</option>

                      <option value="6" {{ ((old('usia') ?? $imunisasi->usia ?? '6') == '6') ? 'selected' : '' }}>Usia 6 Bulan</option>
                      <option value="9" {{ ((old('usia') ?? $imunisasi->usia ?? '9') == '9') ? 'selected' : '' }}>Usia 9 Bulan</option>
                      <option value="12" {{ ((old('usia') ?? $imunisasi->usia ?? '12') == '12') ? 'selected' : '' }}>Usia 1 Tahun</option>
                      <option value="15" {{ ((old('usia') ?? $imunisasi->usia ?? '15') == '15') ? 'selected' : '' }}>Usia 1 Tahun 3 Bulan</option>
                      <option value="18" {{ ((old('usia') ?? $imunisasi->usia ?? '18') == '18') ? 'selected' : '' }}>Usia 1 Tahun 6 Bulan</option>
                      <option value="24" {{ ((old('usia') ?? $imunisasi->usia ?? '24') == '24') ? 'selected' : '' }}>Usia 2 Tahun</option>
                    </select>
                    @if($errors->has('usia'))
                          <div class="text-danger">
                              {{ $errors->first('usia')}}
                          </div>
                     @endif                    
                  </div>

                  <div class="form-group">
                    <label>Max Hari Toleransi (jumlah hari setelah usia ke-n imunisasi masih dimungkinkan)</label>
                    <input type="text" class="form-control" placeholder="" name="max_toleransi" value="{{ old('max_toleransi') ?? $imunisasi->max_toleransi ?? '' }}">
                     @if($errors->has('max_toleransi'))
                          <div class="text-danger">
                              {{ $errors->first('max_toleransi')}}
                          </div>
                     @endif
                  </div>

                  <div class="form-group">
                    <label>Keterangan</label>
                    <textarea class="form-control" name="keterangan">{{ old('keterangan') ?? $imunisasi->keterangan ?? '' }}</textarea>
                     @if($errors->has('keterangan'))
                          <div class="text-danger">
                              {{ $errors->first('keterangan')}}
                          </div>
                     @endif
                  </div>
                                  
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->

          </div>
        </div>
      </div>
    </section>
@endsection
