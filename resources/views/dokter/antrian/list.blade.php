@extends('dokter.layout')
@section('content')
<!-- Content Header (Page header) -->
<style>
   #textarea-resep-container {
   position: relative;
   }
   .float-right {
   float: right;
   }
</style>
<section class="content-header">
   <div class="container-fluid">
      <div class="row mb-2">
         <div class="col-sm-6">
            <h1>Data Antrian</h1>
         </div>
         <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
               <li class="breadcrumb-item"><a href="{{ URL::to('/dokter') }}">Home</a></li>
               <li class="breadcrumb-item active">Data Antrian</li>
            </ol>
         </div>
      </div>
   </div>
   <!-- /.container-fluid -->
</section>
<section class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-12">
            <div class="row">
               <!-- <div class="col-12 col-sm-6 col-md-4">
                  <div class="info-box">
                     <span class="info-box-icon bg-info elevation-1"><i class="fas fa-syringe"></i></span>
                     <div class="info-box-content">
                        <span class="info-box-text">Imunisasi</span>
                        <span class="info-box-number" id="jml_imunisasi">{0 orang}</span>
                     </div>
                     
                  </div>
                  
               </div> -->
               <!-- /.col -->
               <div class="col-12 col-sm-6 col-md-4">
                  <div class="info-box mb-3">
                     <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-user-md"></i></span>
                     <div class="info-box-content">
                        <span class="info-box-text">Ditangani</span>
                        <span class="info-box-number" id="jml_ditangani">{0 orang}</span>
                     </div>
                     <!-- /.info-box-content -->
                  </div>
                  <!-- /.info-box -->
               </div>
               <!-- /.col -->
               <!-- fix for small devices only -->
               <div class="clearfix hidden-md-up"></div>
               <div class="col-12 col-sm-6 col-md-4">
                  <div class="info-box mb-3">
                     <span class="info-box-icon bg-success elevation-1"><i class="fas fa-people-arrows"></i></span>
                     <div class="info-box-content">
                        <span class="info-box-text">Antrian</span>
                        <span class="info-box-number" id="jml_antrian">{0 orang}</span>
                     </div>
                     <!-- /.info-box-content -->
                  </div>
                  <!-- /.info-box -->
               </div>
               <!-- /.col -->
               
            </div>

            <ul id="myTab" class="nav nav-pills" style="padding-bottom: 10px">
               <li class="nav-item">
                     <a href="#antrian" class="nav-link active"  id="tab_antrian">Data Antrian</a>
               </li>
               <li class="nav-item">
                     <a href="#imunisasi" class="nav-link" id="tab_imunisasi">Rekap Imunisasi Hari Ini</a>
               </li>
            </ul>
            <div class="tab-content">
               <div class="tab-pane fade show active" id="antrian">                                        
                  <div class="card">
                     <div class="card-header container-fluid">
                        <div class="row">
                           <div class="col-md-10">
                              <h3 class="card-title">Data Antrian</h3>
                           </div>
                        </div>
                     </div>
                     <!-- /.card-header -->
                     <div class="card-body">
                        <table id="dataTables" class="table table-bordered table-striped">
                           <thead>
                              <tr>
                                 <th>Jenis</th>
                                 <th>Nama Pasien</th>
                                 <th>Tanggal Lahir</th>
                                 <th>Nama Orangtua</th>
                                 <th>Alamat</th>
                                 <th>Riwayat Kesehatan</th>
                                 <th>Opsi</th>
                              </tr>
                           </thead>
                           <tbody id="table-antrian-body">
                              
                           </tbody>
                        </table>
                     </div>
                  <!-- /.card-body -->
                  </div>
               </div>
               <div class="tab-pane fade" id="imunisasi">                    
                  <table id="dataTablesImunisasi" class="table table-bordered table-striped">
                     <thead>
                        <tr>
                           <th>Jenis</th>
                           <th>Jumlah</th>                           
                        </tr>
                     </thead>
                     <tbody id="table-imunisasi-body">
                        
                     </tbody>
                  </table>
               </div>                     
            </div>   
            

            
            <!-- /.card -->
         </div>
      </div>
   </div>
</section>
<div class="modal fade" id="riwayatModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Riwayat Kesehatan</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
         </div>
      </div>
   </div>
</div>
<!-- Modal -->
<div class="modal fade" id="formImunisasi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Pencatatan Data Imunisasi</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <form role="form" method="POST" action="">
               {{ csrf_field() }}
               <input type="hidden" name="pasien_id" id="imunisasi_pasien_id" value="">
               <div class="card-body">
               </div>
               <!-- /.card-body -->
               <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="formPemeriksaan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Pencatatan Data Pemeriksaan</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <form role="form" method="POST" action="">
               {{ csrf_field() }}
               <input type="hidden" name="pasien_id" id="imunisasi_pasien_id" value="">
               <div class="card-body">
                  <div class="form-group">
                     <label>Anamnesa</label>
                     <textarea class="form-control" name="anamnesa" required="">{{ old('anamnesa') }}</textarea>
                     @if($errors->has('anamnesa'))
                     <div class="text-danger">
                        {{ $errors->first('anamnesa')}}
                     </div>
                     @endif      
                  </div>
                  <div class="form-group">
                     <label>Diagnosa</label>
                     <textarea class="form-control" name="diagnosa" required="">{{ old('diagnosa') }}</textarea>
                     @if($errors->has('diagnosa'))
                     <div class="text-danger">
                        {{ $errors->first('diagnosa')}}
                     </div>
                     @endif      
                  </div>
                  <div class="form-group">
                     <label>Tindakan</label>
                     <textarea class="form-control" name="tindakan" required="">{{ old('tindakan') }}</textarea>
                     @if($errors->has('tindakan'))
                     <div class="text-danger">
                        {{ $errors->first('tindakan')}}
                     </div>
                     @endif      
                  </div>
                  <div class="form-group" id="textarea-resep-container">
                     <label>Resep</label>
                     <textarea contenteditable="true" name="resep" cols="40" rows="10" id="textarea-resep" class="form-control">{{ old('resep') }}</textarea>
                     @if($errors->has('resep'))
                     <div class="text-danger">
                        {{ $errors->first('resep')}}
                     </div>
                     @endif      
                  </div>
               </div>
               <!-- /.card-body -->
               <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
<script>

   $("#myTab a").click(function(e){
        $(this).tab('show');
    });

   (function worker() {
      $.ajax({
        url: "{{ URL::to('/dokter/antrian-ajax') }}",
        success: function(data) {
          $('#table-antrian-body').html(data.html);
          $('#table-imunisasi-body').html(data.imunisasi_html);
          $('#jml_imunisasi').html(data.jml_imunisasi);
          $('#jml_ditangani').html(data.jml_ditangani);
          $('#jml_antrian').html(data.jml_antrian);
        },
        complete: function() {
          setTimeout(worker, 5000);
        }
      });
    })();
   
    function showFormImunisasi(pasien_id){
      $.ajax({
          url:"{{ URL::to('/dokter/form-imunisasi-ajax') }}" + "/" + pasien_id,
          success: function(data){
            $('#formImunisasi form .card-body').html(data.html);
           
          }
      });
   
      $('#formImunisasi').modal('show');
      $('#formImunisasi form').attr("action","{{ URL::to('/dokter/imunisasi-simpan') }}" + "/" +  pasien_id);
    }
   
    function showFormPemeriksaan(pasien_id){
      $('#formPemeriksaan').modal('show');
      $('#formPemeriksaan form').attr('action',"{{ URL::to('/dokter/pemeriksaan-simpan') }}" + "/" + pasien_id);
    }
   
   
    // function showRiwayatPemeriksaan(pasien_id){
    //   $.ajax({
    //       url:"{{ URL::to('/dokter/riwayat-pemeriksaan-ajax') }}" + "/"+ pasien_id,
    //       success: function(data){
    //         $('#riwayatModal .modal-body').html(data.html);
           
    //       }
    //   });
   
    //   $('#riwayatModal').modal('show');
    // }
   
    // function showRiwayatImunisasi(pasien_id){
    //   $.ajax({
    //       url:"{{ URL::to('/dokter/riwayat-imunisasi-ajax') }}" + "/"+ pasien_id,
    //       success: function(data){
    //         $('#riwayatModal .modal-body').html(data.html);
           
    //       }
    //   });
   
    //   $('#riwayatModal').modal('show');
    // }
   
   // function showRiwayatAntropometri(pasien_id){
   // $.ajax({
   //        url:"{{ URL::to('/dokter/riwayat-antropometri-ajax') }}" + "/" + pasien_id,
   //        success: function(data){
   //          $('#riwayatModal .modal-body').html(data.html);
           
   //        }
   //    });
   
   //    $('#riwayatModal').modal('show');
   //  }
   
   function showRiwayatKesehatan(pasien_id){
      $.ajax({
          url:"{{ URL::to('/dokter/riwayat-kesehatan-ajax') }}" + "/" + pasien_id,
          success: function(data){
            $('#riwayatModal .modal-body').html(data.html);
           
          }
      });
   
      $('#riwayatModal').modal('show');
    }
   
   
   $(function () {
     
     $('#dataTables,#dataTablesImunisasi').DataTable({
       "paging": true,
       "lengthChange": false,
       "searching": true,
       "ordering": false,
       "info": true,
       "autoWidth": false,
       "responsive": true,
     });
   });
   
   
   
   var tributeAttributes = {
        autocompleteMode: true,
        noMatchTemplate: "",
         values: function (text, cb) {
            remoteSearch(text, users => cb(users));
          },
        selectTemplate: function(item) {
          if (typeof item === "undefined") return null;
          if (this.range.isContentEditable(this.current.element)) {
            return (
              '<span contenteditable="false"><a>' +
              item.original.key +
              "</a></span>"
            );
          }
   
          return item.original.value;
        },
        menuItemTemplate: function(item) {
          return item.string;
        }
      };
   
      function remoteSearch(text, cb) {
        
        if(text.length >= 3){
          var URL = "{{ URL::to('/api/get-daftar-obat') }}";
          xhr = new XMLHttpRequest();
          xhr.onreadystatechange = function() {
            if (xhr.readyState === 4) {
              if (xhr.status === 200) {
                var data = JSON.parse(xhr.responseText);
                cb(data);
              } else if (xhr.status === 403) {
                cb([]);
              }
            }
          };
          xhr.open("GET", URL + "?q=" + text, true);
          xhr.send();  
        }
        
      }
   
      var tributeAutocompleteTestArea = new Tribute(
        Object.assign(
          {
            menuContainer: document.getElementById(
              "textarea-resep-container"
            )
          },
          tributeAttributes
        )
      );
      tributeAutocompleteTestArea.attach(
        document.getElementById("textarea-resep")
      );
</script>
@endsection
