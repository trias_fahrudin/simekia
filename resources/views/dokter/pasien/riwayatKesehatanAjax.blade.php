<div class="row">
   <div class="col-md-12">
      <div class="row">
         <div class="col-md-7">
            <form role="form">
               <div class="form-group">
                  <label for="exampleInputEmail1">
                  Nama
                  </label>
                  <input type="text" class="form-control" disabled="" value="{{ $data_diri->nama }}" />
               </div>
               <div class="form-group">
                  <label for="exampleInputPassword1">
                  Tempat Tanggal Lahir
                  </label>
                  <input type="text" class="form-control" disabled="" value="{{ $data_diri->ttl }}" />
               </div>
               <div class="form-group">
                  <label for="exampleInputFile">
                  Jenis kelamin
                  </label>
                  <input type="text" class="form-control" disabled="" value="{{ $data_diri->jk }}"/>
                  
               </div>
               <div class="form-group">
                  <label for="exampleInputFile">
                  Alamat
                  </label>
                  <textarea class="form-control" disabled="">{{ $data_diri->alamat }}</textarea>                  
               </div>
               
              
            </form>
         </div>
         <div class="col-md-5">
            <img alt="Bootstrap Image Preview" src="{{ URL::to('data_file/' . $data_diri->foto) }}" width="250px" height="250px" />
         </div>
      </div>

      <ul id="myTab" class="nav nav-pills">
          <li class="nav-item">
              <a href="#riwayat_pemeriksaan" class="nav-link active"  id="tab_riwayat_pemeriksaan">Riwayat Pemeriksaan</a>
          </li>
          <li class="nav-item">
              <a href="#riwayat_imunisasi" class="nav-link"  id="tab_riwayat_imunisasi">Riwayat Imunisasi</a>
          </li>
          <li class="nav-item">
              <a href="#bb_u" class="nav-link" id="tab_bb_u">BB/U</a>
          </li>
          <li class="nav-item">
              <a href="#tb_u" class="nav-link"  id="tab_tb_u">TB/U</a>
          </li>        
          <li class="nav-item">
              <a href="#lk_u" class="nav-link"  id="tab_lk_u">LK/U</a>
          </li>        

      </ul>
      <div class="tab-content">
        <div class="tab-pane fade show active" id="riwayat_pemeriksaan">
            
            <table id="dataTablesRiwayatPemeriksaan" class="table table-bordered table-striped">
             <thead>
                <tr>
                   <th>Tanggal</th>
                   <!-- <th>Pelayanan</th> -->
                   <th>Antropometri</th>
                   <th>Diagnosa</th>
                   <th>Tindakan</th>
                   <th>Resep</th>
                </tr>
             </thead>
             <tbody id="table-antrian-body">
                @foreach($riwayat_pemeriksaan as $a)
                <tr>
                   <td>{{ $a->tgl }}</td>
                   <!-- <td>{{ $a->pelayanan }}</td> -->
                   <td>{{ $a->antropometri }}</td>
                   <td>{{ $a->diagnosa }}</td>
                   <td>{{ $a->tindakan }}</td>
                   <td>{{ $a->resep }}</td>
                </tr>
                @endforeach
             </tbody>
          </table>
            
        </div>
        <div class="tab-pane fade" id="riwayat_imunisasi">
            
            <table id="dataTablesRiwayatImunisasi" class="table table-bordered table-striped">
             <thead>
                <tr>
                   <th>Tanggal</th>
                   <!-- <th>Pelayanan</th> -->
                   <th>Antropometri</th>
                   <th>Diagnosa</th>
                   <th>Tindakan</th>
                   <th>Resep</th>
                </tr>
             </thead>
             <tbody id="table-antrian-body">
                @foreach($riwayat_imunisasi as $a)
                <tr>
                   <td>{{ $a->tgl }}</td>
                   <!-- <td>{{ $a->pelayanan }}</td> -->
                   <td>{{ $a->antropometri }}</td>
                   <td>{{ $a->diagnosa }}</td>
                   <td>{{ $a->tindakan }}</td>
                   <td>{{ $a->resep }}</td>
                </tr>
                @endforeach
             </tbody>
          </table>
            
        </div>
        <div class="tab-pane fade" id="bb_u">
            <h5 class="mt-2">Grafik Berat Badan / Usia</h5>
            <div id="chart_bb_u" style="margin-top:20px; margin-left:20px; width:700px; height:600px;"></div>
            {!! $bb_u !!}
            
        </div>
        <div class="tab-pane fade" id="tb_u">
            <h5 class="mt-2">Grafik Tinggi Badan / Usia</h5>
            <div id="chart_tb_u" style="margin-top:20px; margin-left:20px; width:700px; height:600px;"></div> 
            {!! $tb_u !!}
        </div>       
        <div class="tab-pane fade" id="lk_u">
            <h5 class="mt-2">Grafik Lingkar Kepala / Usia</h5>
            <div id="chart_lk_u" style="margin-top:20px; margin-left:20px; width:700px; height:600px;"></div> 
            {!! $lk_u !!}
        </div>       
      </div>

      
   </div>
</div>
<script type="text/javascript">
   


    $("#myTab a").click(function(e){
        $(this).tab('show');
    });

    $('.nav-pills a').on('show.bs.tab', function(){
    // alert('The new tab is about to be shown.');
    });

    $('.nav-pills a').on('shown.bs.tab', function(){
        //alert('The new tab is now fully shown.');
        if($(this).attr('id') == "tab_bb_u"){
            console.log($(this).attr('id'));
            plot_bb_u.replot();
        }else if($(this).attr('id') == "tab_tb_u"){
            console.log($(this).attr('id'));
            plot_tb_u.replot();
        }else if($(this).attr('id') == "tab_lk_u"){
            console.log($(this).attr('id'));
            plot_lk_u.replot();
        }
    });

      $('.nav-pills a').on('hide.bs.tab', function(e){
        // alert('The previous tab is about to be hidden.');
      });

      $('.nav-pills a').on('hidden.bs.tab', function(){
        // alert('The previous tab is now fully hidden.');
      });





   

   $(function () {     
        $('#dataTablesRiwayatPemeriksaan, #dataTablesRiwayatImunisasi').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": true,
          "ordering": false,
          "info": true,
          "autoWidth": false,
          "responsive": true,
           "pageLength": 5
        });
     });
</script>