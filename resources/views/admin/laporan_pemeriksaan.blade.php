@extends('admin.layout')
@section('content')

<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

<!-- Content Header (Page header) -->
<section class="content-header">
   <div class="container-fluid">
      <div class="row mb-2">
         <div class="col-sm-6">
            <h1>Laporan Pemeriksaan</h1>
         </div>
         <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
               <li class="breadcrumb-item"><a href="{{ URL::to('/admin') }}">Home</a></li>
               <li class="breadcrumb-item active"><a href="{{ URL::to('/admin/laporan-pemeriksaan') }}">Laporan Pemeriksaan</a></li>
            </ol>
         </div>
      </div>
   </div>
   <!-- /.container-fluid -->
</section>
<section class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-12">

            <div class="card card-primary">
               <div class="card-header">
                  <h3 class="card-title">Laporan Pemeriksaan</h3>
               </div>
               <!-- /.card-header -->
               <!-- form start -->
               <form role="form" method="POST" action="{{ URL::to('/admin/laporan-pemeriksaan') }}">
                  {{ csrf_field() }}
                  <div class="card-body">
                     <div class="form-group">
                                 <label>Filter Tanggal</label>
                                 <input type="text" name="filter_tgl" class="form-control" value="<?php echo $filter_tgl?>" />
                     </div>
                     <script>
                     $(function() {
                        $('input[name="filter_tgl"]').daterangepicker({
                           opens: 'left',
                           locale: {
                              format: 'YYYY-MM-DD',
                              separator: " s/d ",
                           }
                        }, function(start, end, label) {
                           console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
                        });
                     });
                     </script>
                  </div>
                  <!-- /.card-body -->
                  <div class="card-footer">
                     <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
               </form>
            </div>
            
            
            <table id="dataTables" class="table table-bordered table-striped">                        
               <thead>
                   <tr>
                     <th>Nama</th>
                     <th>Tanggal</th>
                     <th>Anamnesa</th>
                     <th>Gender</th>
                     <th>Usia (Bulan)</th>
                     <!-- <th>Diagnosa</th>
                     <th>Tindakan</th>                              
                     <th>Resep</th>                               -->
                   </tr>
               </thead>
               <tbody>
                   @foreach($data as $o)
                   <tr>
                     <td>{{ $o->nama_pasien }}</td>
                     <td>{{ $o->tgl }}</td>
                     <td>{{ $o->anamnesa }}</td>
                     <td>{{ $o->jk }}</td>
                     <td>{{ $o->usia }}</td>
                     <!-- <td>{{ $o->diagnosa }}</td>
                     <td>{{ $o->tindakan }}</td>                              
                     <td>{{ $o->resep }}</td>                               -->
                   </tr>
                   @endforeach
               </tbody>
             </table>
             
         </div>
      </div>
   </div>
</section>

<script>
   $(function () {
     
     $('#dataTables').DataTable({
       "paging": true,
       "lengthChange": false,
       "searching": true,
       "ordering": true,
       "info": true,
       "autoWidth": false,
       "responsive": true,
     });
   });
</script>

@endsection