SELECT b.nama AS nama_anak,
       b.tgl_lahir AS tgl_lahir, 
		 b.jk AS gender, 
		 a.tgl AS tgl_periksa, 
		 a.berat_badan,
		 a.tinggi_badan,
		 a.lingkar_kepala, 
  		 TIMESTAMPDIFF(MONTH,b.tgl_lahir,a.tgl) AS month_diff,
		 (CASE WHEN a.berat_badan < c.min_3_sd THEN 'Sangat Kurus' 
		       WHEN a.berat_badan >= c.min_3_sd AND a.berat_badan < c.min_2_sd THEN 'Kurus' 
				 WHEN a.berat_badan >= c.min_2_sd AND a.berat_badan <= c.plus_2_sd THEN 'Normal' 
				 WHEN a.berat_badan > c.plus_2_sd THEN 'Gemuk' 
				 ELSE 'data-belum-ada' 
		  END) AS status_bb_pb,
		 (CASE WHEN a.berat_badan < d.min_3_sd THEN 'Sangat Kurus' 
		       WHEN a.berat_badan >= d.min_3_sd AND a.berat_badan < d.min_2_sd THEN 'Kurus' 
				 WHEN a.berat_badan >= d.min_2_sd AND a.berat_badan <= d.plus_2_sd THEN 'Normal' 
				 WHEN a.berat_badan > d.plus_2_sd THEN 'Gemuk' 
				 ELSE 'data-belum-ada' 
		  END) AS status_bb_tb,
		  (CASE WHEN a.berat_badan < e.min_3_sd THEN 'Gizi Buruk' 
		       WHEN a.berat_badan >= e.min_3_sd AND a.berat_badan < e.min_2_sd THEN 'Gizi Kurang' 
				 WHEN a.berat_badan >= e.min_2_sd AND a.berat_badan <= e.plus_2_sd THEN 'Gizi Baik' 
				 WHEN a.berat_badan > e.plus_2_sd THEN 'Gizi Lebih' 
				 ELSE 'data-belum-ada' 
		  END) AS status_bb_u,
		  (CASE WHEN (a.berat_badan/((a.tinggi_badan/100) * (a.tinggi_badan/100))) < f.min_3_sd THEN 'Sangat Kurus' 
		        WHEN (a.berat_badan/((a.tinggi_badan/100) * (a.tinggi_badan/100))) >= f.min_3_sd AND (a.berat_badan/((a.tinggi_badan/100) * (a.tinggi_badan/100))) < f.min_2_sd THEN 'Kurus' 
				  WHEN (a.berat_badan/((a.tinggi_badan/100) * (a.tinggi_badan/100))) >= f.min_2_sd AND (a.berat_badan/((a.tinggi_badan/100) * (a.tinggi_badan/100))) <= f.plus_2_sd THEN 'Normal' 
				  WHEN (a.berat_badan/((a.tinggi_badan/100) * (a.tinggi_badan/100))) > f.plus_2_sd THEN 'Gemuk' 
				  ELSE 'data-belum-ada' 
			END) AS status_imt_u_0_60,
			(CASE WHEN (a.berat_badan/((a.tinggi_badan/100) * (a.tinggi_badan/100))) < g.min_3_sd THEN 'Sangat Kurus' 
		        WHEN (a.berat_badan/((a.tinggi_badan/100) * (a.tinggi_badan/100))) >= g.min_3_sd AND (a.berat_badan/((a.tinggi_badan/100) * (a.tinggi_badan/100))) < g.min_2_sd THEN 'Kurus' 
				  WHEN (a.berat_badan/((a.tinggi_badan/100) * (a.tinggi_badan/100))) >= g.min_2_sd AND (a.berat_badan/((a.tinggi_badan/100) * (a.tinggi_badan/100))) <= g.plus_1_sd THEN 'Normal' 
				  WHEN (a.berat_badan/((a.tinggi_badan/100) * (a.tinggi_badan/100))) > g.plus_1_sd  AND (a.berat_badan/((a.tinggi_badan/100) * (a.tinggi_badan/100))) <= g.plus_2_sd THEN 'Gemuk' 
				  WHEN (a.berat_badan/((a.tinggi_badan/100) * (a.tinggi_badan/100))) > g.plus_2_sd THEN 'Obesitas' 
				  ELSE 'data-belum-ada' 
			END) AS status_imt_u_61_72,
  		  (CASE WHEN a.lingkar_kepala < h.min_1_sd THEN 'Mikrosefalus' 
			     WHEN a.lingkar_kepala >= h.min_1_sd AND a.lingkar_kepala <= h.plus_1_sd THEN 'Normal' 
				  WHEN a.lingkar_kepala > h.plus_1_sd THEN 'Makrosefalus' 
				  ELSE 'data-belum-ada' 
			END) AS status_lk_u,
 			(CASE WHEN a.tinggi_badan < i.min_3_sd THEN 'Sangat Pendek' 
			      WHEN a.tinggi_badan >= i.min_3_sd AND a.tinggi_badan < i.min_2_sd THEN 'Pendek' 
					WHEN a.tinggi_badan >= i.min_2_sd AND a.tinggi_badan <= i.plus_2_sd THEN 'Normal' 
					WHEN a.tinggi_badan > i.plus_2_sd THEN 'Tinggi' 
					ELSE 'data-belum-ada' 
			END) AS status_pb_u,
			(CASE WHEN a.tinggi_badan < j.min_3_sd THEN 'Sangat Pendek' 
			      WHEN a.tinggi_badan >= j.min_3_sd AND a.tinggi_badan < j.min_2_sd THEN 'Pendek' 
					WHEN a.tinggi_badan >= j.min_2_sd AND a.tinggi_badan <= j.plus_2_sd THEN 'Normal' 
					WHEN a.tinggi_badan > j.plus_2_sd THEN 'Tinggi' 
					ELSE 'data-belum-ada' 
			END) AS status_tb_u
FROM antropometri a
LEFT JOIN pasien b ON a.pasien_id = b.id
LEFT JOIN standart_bb_pb c ON (FLOOR(a.tinggi_badan * 2  + 0.5) / 2) = c.panjang_badan AND IF(b.jk='L','Laki-laki','Perempuan') = c.jk
LEFT JOIN standart_bb_tb d ON (FLOOR(a.tinggi_badan * 2  + 0.5) / 2) = d.tinggi_badan AND IF(b.jk='L','Laki-laki','Perempuan') = d.jk
LEFT JOIN standart_bb_u e ON TIMESTAMPDIFF(MONTH,b.tgl_lahir,a.tgl) = e.umur_bulan AND IF(b.jk='L','Laki-laki','Perempuan') = e.jk
LEFT JOIN standart_imt_u f ON TIMESTAMPDIFF(MONTH,b.tgl_lahir,a.tgl) = f.umur_bulan AND IF(b.jk='L','Laki-laki','Perempuan') = f.jk
LEFT JOIN standart_imt_u g ON TIMESTAMPDIFF(MONTH,b.tgl_lahir,a.tgl) = g.umur_bulan AND IF(b.jk='L','Laki-laki','Perempuan') = g.jk
LEFT JOIN standart_lk_u h ON TIMESTAMPDIFF(MONTH,b.tgl_lahir,a.tgl) = h.umur_bulan AND IF(b.jk='L','Laki-laki','Perempuan') = h.jk
LEFT JOIN standart_pb_u i ON TIMESTAMPDIFF(MONTH,b.tgl_lahir,a.tgl)  = i.umur_bulan AND IF(b.jk='L','Laki-laki','Perempuan') = i.jk
LEFT JOIN standart_tb_u j ON TIMESTAMPDIFF(MONTH,b.tgl_lahir,a.tgl)  = j.umur_bulan AND IF(b.jk='L','Laki-laki','Perempuan') = j.jk
ORDER BY a.tgl DESC